#include <stdlib.h>
#include <stdio.h> 
#include <unistd.h>
#include <string.h>
#include <sys/resource.h>

#define TRUE 1
#define FALSE 0


int main(int argc, char * argv[]) 
{ 
	int *a[10];

	for(int i=0;i<10;i++){
		a[i] = (int*)malloc(1e7);
		memset(a[i],0,1);
		struct rusage usage;
		getrusage(RUSAGE_SELF, &usage);

		printf("Memory usage - %ld\n", usage.ru_maxrss);

		sleep(1);
	}
	
	return(0);
}
