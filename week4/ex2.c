#include <stdio.h>
#include <stdlib.h>

int main(void) {
 	
 	//Each fork call creates new process which goes from the same point
 	for(int i=0;i<5;i++){
 		fork();
 	}
 	sleep(5);


 return 0;
}