#include <stdio.h>
#include <stdlib.h>

int main(void) {
 	int n = getpid();
 	int pid = fork(); 
 	if (pid != 0) 
 		printf("Hello from parent [%d - %d]\n", pid, n);
 	else if (pid ==0) //pid variable of the child process is set to 0 by fork call
 		printf("Hello from child [%d - %d]\n", pid, n);
	else 
 		return 1;//error
 return 0;
}