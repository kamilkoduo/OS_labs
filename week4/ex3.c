#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

int readcom(char *command){
	fgets(command, sizeof(command), stdin);
	return *(command+strlen(command)-2) == '&';	 
}

//print symbols before command in shell
void print_start() {
 printf("@ ");
} 

int main(void) {
 	char command[20] = "";
 	int k;
 	while(1){
 		print_start();
		k = readcom(command);
 		if(k){
 			int pid = fork();
 			if(pid == 0){
 				
 				system(command);
 				return 0;
 			}
 		}
 		else 
 			system(command);
  	}

 return 0;
}