If we want to increase hit/miss ratio, we need to decrease miss to its minimum, which is 1.
The minimum miss is 1 because any page is not in memory at the initial state.
Hit number will be maximized automatically, because it is (n-miss)
So we if we have 1 miss out of n references, we have (n-1)/1 hit/miss ratio.

Example, 1 1 1 1 1 1 1 1 ... 1 (n times '1')



If we want to decrease hit/miss ratio we need to minimize hit and maximize miss, so every reference should result in miss. To sum up, hit number equals to 0 and we have ratio 0/n = 0

Example, 1 2 3 4 5 6 7 8 9 10 11 ... n (n different references)