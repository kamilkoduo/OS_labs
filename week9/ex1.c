#include <stdio.h>
#include <stdlib.h>

#define NIL -1
#define TICKS 32

typedef struct {
	unsigned long long counter : TICKS;
	int used : 1; 
	int reference;
} page_frame;

float ageing(FILE *fp, int k){
	int hit =0;
	int all = 0;
	page_frame* pf = malloc(sizeof(page_frame)*k);
	for(int i=0;i<k;i++){
		pf[i].counter=NIL;
		pf[i].used =0;
		pf[i].reference=NIL;
	}

	while(!feof(fp)){
		int ref;
		fscanf(fp, "%d", &ref);

		all++;

		int flag = 1;
		int min_ind=0;
		for(int i =0;i<k;i++){
			pf[i].counter >>= 1;
			if(ref == pf[i].reference){
					pf[i].counter |= (1<<(TICKS-1));
					flag=0;
					hit++;
				}

			if(pf[i].used == 0 ){
				min_ind = i;
				break;
			}
			if(pf[i].counter < pf[min_ind].counter)
				min_ind = i;
		}

		if(flag){
			pf[min_ind].counter = 1<<(TICKS-1);
			pf[min_ind].reference=ref;
			pf[min_ind].used = 1;
		}
	}

	printf("%d %d\n", hit, all);
	return (float)(hit)/(float)(all-hit);
}


int main(int argc, char *argv[]){

	int k = 0;
	
	if(argc !=2){
		scanf("%d", &k);
	}else
		k = atoi(argv[1]); 

	FILE *fp = fopen("e1input.txt", "r");
	float res = ageing(fp, k);
	fclose(fp);

	printf("%f\n", res);

	return 0;
}