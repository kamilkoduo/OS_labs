#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define N 100


typedef struct {
	int inum;
	char** names;
	int ncount;
} ninum;


int contains(ninum *array, int n, int inum){
	for(int i=0;i<n;i++){
		if(inum == array[i].inum)
			return i;
	}
	return -1;
}

int main(int argc, char *argv[]){
	
	DIR *tmp = malloc(sizeof(DIR *));
	tmp= opendir("tmp");
	
	ninum* ninums = malloc(sizeof(ninum)*N);
	int g=0;
	

	struct dirent *dir= readdir(tmp);
	while(dir !=NULL){
		char *name = dir->d_name;
		char *path = "./tmp/";
		char *pname = malloc(sizeof(char)*(strlen(name)+strlen(path)));

		strcpy(pname, path);
		strncat(pname, name, strlen(name));

		struct stat *st = malloc(sizeof(struct stat *));
		stat(pname, st);

		int inum = (int) st->st_ino;
		int t=contains(ninums, g, inum);
		
		if(t!=-1){
			ninums[t].names[ninums[t].ncount]=pname;
			ninums[t].ncount++;

		}else{
			ninums[g].inum=inum;
			ninums[g].ncount=0;

			ninums[g].names = malloc(sizeof(char)*N);
			ninums[g].names[ninums[g].ncount]=pname;

			ninums[g].ncount++;
			g++;
		}
		dir = readdir(tmp);
	}

	for(int i=0;i<g;i++){
		if(ninums[i].ncount>1){
			printf("inum:%d count:%d\nLinks:\n", ninums[i].inum,ninums[i].ncount);
			for(int j=0;j<ninums[i].ncount;j++){
				printf("#%d: %s\n", j,ninums[i].names[j]);
			}
			printf("\n");
		}
	}
	return 0;
}