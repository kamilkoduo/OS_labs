
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define TRUE 1
#define FALSE 0


int main(int argc, char * argv[]) 
{ 
    int fd[2];
    pipe(fd);

    char* str1 = "OpSystems";
    char* str2 = malloc(sizeof(char)*20);

    pid_t child_pid = fork();

    if(child_pid){
        printf("Hi, I am  process that writes to the pipe. My pid is: %ld\n", (long int) getpid());
        write(fd[1], str1, (strlen(str1)+1));
    }
    else{
        sleep(3);
        printf("Hi, I am  process that reads from the pipe. My pid is: %ld\n", (long int) getpid());
        read(fd[0], str2, 20);
        printf("Received string: %s\n", str2);
    }

    return(0);
}