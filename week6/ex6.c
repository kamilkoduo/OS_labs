#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

#define TRUE 1
#define FALSE 0

#define STATE 10


int main(int argc, char * argv[]) 
{ 

    int fd[2];
    pipe(fd);

    pid_t child_pid1=0;
    pid_t child_pid2=0;
    
    child_pid1 = fork();

    int status;



    if(child_pid1){
        child_pid2 = fork();
        //main
        if(child_pid2){
            sleep(1);
            close(fd[0]);
            write(fd[1], &child_pid2, sizeof(pid_t));
            close(fd[1]);
            printf("child_pid2 %ld was sent\n...Waiting\n", (long int)child_pid2);
            pid_t terminated = waitpid(child_pid2, &status, WUNTRACED);
            
            sleep(2);
            printf("Main exits with status 0\n");
            exit(0);
        }

        //child 2
        else{
            while(TRUE){
                close(fd[0]);
                close(fd[1]);
                printf("Hi, I am child 2 with pid=%ld\n", (long int) getpid());
                sleep(1);
            }
        }
    }
    //child 1
    else{
        close(fd[1]);
        while(TRUE){
            printf("Hi, I am child 1 with pid=%ld\n", (long int) getpid());
            sleep(5);
            pid_t child_pid;
            read(fd[0], &child_pid, sizeof(pid_t));
            if(child_pid){
                close(fd[0]);
                printf("Stop child 2 with pid=%ld\n", (long int) child_pid);
                kill(child_pid, SIGSTOP);
            
                printf("Child 1 exits with status 0\n");
                exit(0);
            }
        }
    }

    return(0);
}