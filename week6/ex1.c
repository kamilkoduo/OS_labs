
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define TRUE 1
#define FALSE 0


int main(int argc, char * argv[]) 
{ 
    int fd[2];
    pipe(fd);

    char* str1 = "OpSystems";
    char* str2 = malloc(sizeof(char)*20);


    printf("String 1: %s\n", str1);
    printf("String 2: %s\n...Do write and read\n", str2);

    write(fd[1], str1, (strlen(str1)+1));
    read(fd[0], str2, 20);

    printf("Received string: %s\n", str2);

    return(0);
}