#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

#define TRUE 1
#define FALSE 0


int main(int argc, char * argv[]) 
{ 
    pid_t child_pid = fork();

    if(child_pid){
        sleep(10);
        printf("Process with pid=%ld is sending SIGTERM signal to child with pid=%ld\n",
            (long int)getpid(), (long int)child_pid);
        kill(child_pid, SIGTERM);
    }
    else{
        while(TRUE){
            printf("I'm alive\n");
            sleep(1);
        }
    }

    return(0);
}