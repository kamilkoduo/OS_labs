
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

#define TRUE 1
#define FALSE 0

void sigkill_func(int arg){
	printf("SIGKILL signal was handled\n");
}

void sigstop_func(int arg){
	printf("SIGSTOP signal was handled\n");
}

void sigusr1_func(int arg){
	printf("SIGUSR1 signal was handled\n");}

int main(int argc, char * argv[]) 
{ 
    signal(SIGKILL, sigkill_func);
    signal(SIGSTOP, sigstop_func);
    signal(SIGUSR1, sigusr1_func);

    while(TRUE){
        sleep(1);
    }

    return(0);
}