
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

#define TRUE 1
#define FALSE 0

void sigint_func(int arg){
    printf("\nStill doubts? Play more...\n");
}

int main(int argc, char * argv[]) 
{ 
    signal(SIGINT, sigint_func);
    printf("Hi, I am that program which handles Ctrl-C\nDon't you believe me? Huh, try yourself!\n");

    while(TRUE){
        sleep(1);
    }

    return(0);
}