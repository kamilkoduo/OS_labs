#include <stdlib.h>
#include <stdio.h> 

#define TRUE 1
#define FALSE 0


int main(int argc, char * argv[]) 
{ 
	
	int n;
	scanf("%d", &n);
	int* a = malloc(n*sizeof(int));
	for(int i=0;i<n;i++){
		a[i]=i;
	}
	for(int i=0;i<n;i++){
		printf("%d ",a[i]);
	}
	free(a);
	
	return(0);
}
