#include <stdlib.h>
#include <stdio.h>


int* reallocate(int* ptr, size_t size){
	int* np = malloc(sizeof(int)*size);
	
	if(ptr!=NULL){
		for(int i=0;i<size;i++){
			np[i]=*(ptr+i);
		}
		free(ptr);
	}
	return np;
}
int main(){
	
	int* a= malloc(5*sizeof(int));
	for(int i=0;i<5;i++){
		a[i]=5;
	}

	a =reallocate(a, 10);
	for(int i=0;i<10;i++){
		printf("%d ",a[i]);
	}
		
	return 0;
}
