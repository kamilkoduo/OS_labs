#!/bin/bash

while [[ -f ex2.lock ]]; do
	sleep 2
done
ln ex2.txt ex2.lock

echo "start" >> ex2.lock

for i in 1 2 3 4 5
do  
	number=$(grep -Eo '^[0-9]+$' ex2.txt | tail -1)
	number=$((number+1))
	echo $number >> ex2.lock
	sleep 2

done


echo "end" >> ex2.lock

rm ex2.lock