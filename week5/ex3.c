#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h> 
#include <pthread.h> 
#include <unistd.h>

#define BUFFER_SIZE 5
#define EMPTY 1
#define FULL 2

pthread_cond_t NOT_EMPTY, NOT_FULL;

int count = 4;
pthread_mutex_t mvar=PTHREAD_MUTEX_INITIALIZER;

int buffer[BUFFER_SIZE] = {0,1,2,3,4};

void *producer(void *args){
    int ind;
    while(1){
        sleep(2);

        ind = count;
        ind++;

        if(ind==BUFFER_SIZE-1){//nothing to be produced 
            pthread_cond_wait(&NOT_FULL,&mvar);
        }
        
        *(buffer+ind) = ind;
        
        if(ind>=0){//can be consumed
            pthread_cond_signal(&NOT_EMPTY);
        }

        printf("producer introduces item %d\n", ind);
        count++;
    }
}

void *consumer(void *args){
    int item;
    int ind;
    while(1){
        sleep(1);
        ind = count;

        if(ind==-1){//nothing to consume
            pthread_cond_wait(&NOT_EMPTY,&mvar);
        }
        item = *(buffer+ind);
        *(buffer+ind) = -1;
        ind--;
        if(ind<BUFFER_SIZE-1){//can be produced 
            pthread_cond_signal(&NOT_FULL);
        }
        printf("consumer consumes item %d\n", item);
        count--;
    }
}

int main(int argc, char * argv[]) 
{ 
    //buffer = (int*)malloc(sizeof(int)*BUFFER_SIZE);
    
    pthread_t consumer_id, producer_id;
    pthread_create(&producer_id, NULL, producer,NULL);
    pthread_create(&consumer_id, NULL, consumer,NULL);


    pthread_join(producer_id, NULL);

	pthread_exit(NULL);
}