
#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h> 
#include <unistd.h>

#define NUM_THREADS 5

void *thread_function(void *args) 
{ 
	int k = *(int *)args;

	
    printf("Thread #%d with id %ld is running\n",
    	k,(long int) pthread_self()); 
    sleep(5);
   	printf("Thread #%d terminates\n", k);
   	
    pthread_exit(NULL);
} 
   
int main(int argc, char * argv[]) 
{ 
	int rc, i;
	pthread_t thread_id[NUM_THREADS];

	int n;
	scanf("%d", &n);
    
    for(int i=0;i<NUM_THREADS;i++){
    	printf("\nMain thread with id %ld creates a thread %ld in iteration #%d\n",
    		(long int)pthread_self(), (long int)*(thread_id+i), i); 
    	rc = pthread_create(thread_id+i, NULL, thread_function, (int*)&i); 
    	if(rc){
    		printf("\n ERROR: return code from pthread_create is %d \n", rc);
			exit(1);
    	}
    	if(n){
    		pthread_join(*(thread_id+i), NULL); 
    	} 
    	sleep(2);
	}

	pthread_exit(NULL);
}