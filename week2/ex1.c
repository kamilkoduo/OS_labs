#include <stdio.h>
#include <values.h>

int main() {
    int i = INT_MAX;
    float f = MAXFLOAT;
    double d = MAXDOUBLE;
    printf("INT_MAX %d INT_SIZE %d \n", i,(int)sizeof(typeof(i)));
    printf("MAXFLOAT %f FLOAT_SIZE %d \n",f, (int)sizeof(typeof(f)));
    printf("MAXDOUBLE %f DOUBLE_SIZE %d \n",d, (int)sizeof(typeof(d)));
    return 0;
}
