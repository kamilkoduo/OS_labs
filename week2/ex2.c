#include <string.h>
#include <stdlib.h>
#include <stdio.h>
char* reverse(char *string){
    int l = strlen(string);
    char *res = malloc(l* sizeof(char));
    for(int i=0;i<l;i++){
        *(res+i) = *(string+l-1-i);
    }
    return res;
}
int main() {
    char *string = malloc(sizeof(char) * 256);

    scanf("%s", string);

    printf("%s \n", reverse(string));
    return 0;
}
