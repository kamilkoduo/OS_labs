#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void triangle(int n) {
    for (int i = 1; i < n + 1; i++) {
        for (int j = 0; j < n - i; j++) {
            printf("%c", ' ');
        }
        for (int j = 0; j < 2 * i - 1; j++) {
            printf("%c", '*');
        }
        printf("\n");
    }
}
void triangle2(int n) {
    for (int i = 1; i < n + 1; i++) {
        for (int j = 0; j < i; j++) {
            printf("%c", '*');
        }
        printf("\n");
    }
}
void rectangle(int n){
    for (int i = 1; i < n + 1; i++) {
        for (int j = 0; j < n; j++) {
            printf("%c", '*');
        }
        printf("\n");
    }
}


int main() {
    int n;
    int k;
    printf("CHOOSE: \n 1-triangle 1 \n 2-triangle 2 \n 3-rectangle\n");
    scanf("%d %d", &k,&n);
    if(k==1)
        triangle(n);
    if(k==2)
        triangle2(n);
    if(k==3)
        rectangle(n);
    return 0;
}
